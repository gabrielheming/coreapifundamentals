using System.Threading.Tasks;
using AutoMapper;
using CoreCodeCamp.Data;
using CoreCodeCamp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace CoreCodeCamp.Controllers
{
    [ApiController]
    [Route("api/camps/{moniker}/talks")]
    public class TalksController : ControllerBase
    {
        private readonly ICampRepository repository;
        private readonly IMapper mapper;
        private readonly LinkGenerator liknGenerator;

        public TalksController(ICampRepository repository, IMapper mapper, LinkGenerator linkGenerator)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.liknGenerator = linkGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<TalkModel[]>> GetTalks(string moniker)
        {
            try
            {
                var talks = await repository.GetTalksByMonikerAsync(moniker, true);

                return mapper.Map<TalkModel[]>(talks);
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Database Failure {ex.Message}");
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TalkModel>> GetTalk(string moniker , int id)
        {
            try
            {
                var talk = await repository.GetTalkByMonikerAsync(moniker, id, true);

                return mapper.Map<TalkModel>(talk);
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Database Failure {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<ActionResult<TalkModel>> Post(string moniker, TalkModel model)
        {
            try
            {
                Camp camp = await repository.GetCampAsync(moniker);

                if (camp == null)
                {
                    return BadRequest("Camp does not exists");
                }

                Talk talk = mapper.Map<Talk>(model);
                talk.Camp = camp;

                if (model.Speaker == null)
                {
                    return BadRequest("Speaker ID is required");
                }

                Speaker speaker = await repository.GetSpeakerAsync(model.Speaker.Id);
                if (speaker == null)
                {
                    return BadRequest("Speaker could no be found");
                }

                talk.Speaker = speaker;

                repository.Add(talk);

                if (await repository.SaveChangesAsync())
                {
                    var location = liknGenerator.GetPathByAction(
                        HttpContext, 
                        "GetTalk",
                        values: new { moniker, id = talk.TalkId }
                    );

                    return Created(location, mapper.Map<TalkModel>(talk));
                }
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Database Failure {ex.Message}");
            }

            return BadRequest("Failed to save new Talks");
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<TalkModel>> Put(string moniker, int id, TalkModel model)
        {
            try
            {
                Talk talk = await repository.GetTalkByMonikerAsync(moniker, id, true);
                if (talk == null)
                {
                    return NotFound("Talk not found");
                }

                if (model.Speaker != null)
                {
                    Speaker speaker = await repository.GetSpeakerAsync(model.Speaker.Id);

                    if (speaker != null)
                    {
                        talk.Speaker = speaker;
                    }
                }

                mapper.Map(model, talk);

                if (await repository.SaveChangesAsync())
                {
                    return mapper.Map<TalkModel>(talk);
                }


            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Database Failure {ex.Message}");
            }

            return BadRequest("Failed to update Talk");
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(string moniker, int id)
        {
            try
            {
                Talk talk = await repository.GetTalkByMonikerAsync(moniker, id);
                if (talk == null)
                {
                    return NotFound("Failed to find Talk to delete");
                }

                repository.Delete(talk);

                if (await repository.SaveChangesAsync())
                {
                    return Ok();
                }                
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Database Failure {ex.Message}");
            }

            return BadRequest("Failed to delete Talk");
        }
    }
}
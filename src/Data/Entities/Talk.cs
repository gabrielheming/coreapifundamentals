﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoreCodeCamp.Data
{
  public class Talk
  {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int TalkId { get; set; }
    public Camp Camp { get; set; }
    public string Title { get; set; }
    public string Abstract { get; set; }
    public int Level { get; set; }
    public Speaker Speaker { get; set; }
  }
}
using AutoMapper;
using CoreCodeCamp.Models;

namespace CoreCodeCamp.Data
{
    public class SpeakerProfile : Profile
    {
        public SpeakerProfile()
        {
            this.CreateMap<Speaker, SpeakerModel>()
                .ForMember(sm => sm.Id , o => o.MapFrom(s => s.SpeakerId))
                .ReverseMap();
        }
    }
}